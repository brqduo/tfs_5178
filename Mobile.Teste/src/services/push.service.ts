import { TokenService } from '@ons/ons-mobile-login';
import { Injectable } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Firebase } from '@ionic-native/firebase';
import { tap } from 'rxjs/operators';
import { Config } from './../environment/config';
import { ToastController } from 'ionic-angular';

@Injectable()
export class PushService {
  private _appV: any = '';

  constructor(
    private httpClient: HttpClient,
    private appVersion: AppVersion,
    private alertCtrl: AlertController,
    private device: Device,
    private platform: Platform,
    private fcmSrv: Firebase,
    private tokenSrv: TokenService,
    private toastCtrl: ToastController
  ) {

  }


  checkCordova() {
    if (this.platform.is('Cordova')) {
      this.appVersion.getVersionNumber().then((x: string) => {
        this.appV = x;
      })
      this.ativarPushListener()
    } else {
      this.alertCtrl.create({
        title: Config.MensagemGenerica.PUSH_BROWSER_ERROR_TITLE,
        subTitle: Config.MensagemGenerica.PUSH_BROWSER_ERROR,
        buttons: ['OK']
      }).present();
    }
  }
  public set appV(data) {
    this._appV = data
  }

  public get appV() {
    return this._appV
  }

  async registrarFirebase() {
    if (this.platform.is('Cordova')) {
      const headers = new HttpHeaders(
        {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer ' + this.tokenSrv.getToken().access_token
        }
      );
      const options = { headers };

      const body = JSON.stringify({
        "appVersao": this.appV,
        "dispositivoId": await this.fcmSrv.getToken(),//this.device.uuid,
        "dispositivoDetalhe": {
          "so": this.device.platform,
          "modelo": this.device.model
        },
        "topicos": [
          "Publicações"
        ]
      });
      this.httpClient.post(Config.FIREBASE_URL, body, options)
        .subscribe(data => {
          console.log('Registro funcinou e me retornou isso:', data);
          return data;
        });
    } else {
      console.info('O Q ESTA ACONTECENDO AQUI!!?!?');
      this.alertCtrl.create({
        title: Config.MensagemGenerica.PUSH_BROWSER_ERROR_TITLE,
        subTitle: Config.MensagemGenerica.PUSH_BROWSER_REGISTER_ERROR,
        buttons: ['OK']
      }).present();
    }
  }
  ativarPushListener() {
    this.fcmSrv.onNotificationOpen().pipe(
      tap((msg) => {
        this.showPushAlert(msg.TipoMSG)
      })).subscribe((data) => {
        console.log('o q estou recebendo de fato?', data)
      })
  };
  showPushAlert(msgType) {
    switch (msgType) {
      case Config.ANIVERSARIANTE:
        const aniversario = this.alertCtrl.create({
          title: 'FELIZ ANIVERSARIO!',
          subTitle: 'O Ons lhe deseja um feliz aniversario e um excelente dia'
        });
        aniversario.present();
        break;
      case Config.COORPORATIVA:
        const corporativa = this.alertCtrl.create({
          title: 'Items de reunião',
          subTitle: 'body da mensagem corporativa'
        });
        corporativa.present();
        break;
      default:
        break;
    }


  }
}
