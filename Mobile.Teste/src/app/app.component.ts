//import { UtilService } from './../services/util.service';
import { Component } from '@angular/core';
// import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginService, ErrorService, EnvironmentService, NetWorkService } from '@ons/ons-mobile-login';
import { LoginPage, ImageService, UtilService } from '@ons/ons-mobile-login';
// import { Container } from '@angular/compiler/src/i18n/i18n_ast';
import { ContatosPage } from '../pages/contatos/contatos';
import { PushService } from '../services/push.service';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(platform: Platform,
    statusBar: StatusBar,
    public loginSrv: LoginService,
    splashScreen: SplashScreen,
    public imageSrv: ImageService,
    public envSrv: EnvironmentService,
    public utilSrv: UtilService,
    public errorSrv: ErrorService,
    public networkSrv: NetWorkService,
    public pushSrv: PushService,
    public app: App) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      imageSrv.SetDefault();
      loginSrv.setAplicationName('Mobile.Contatos');
      envSrv.setEnv('TST');
      this.rootPage = LoginPage;
      this.loginSrv.onConectedChange
        .subscribe((u: any) => {
          if (u !== undefined) {
            if (u.Connected) {
              this.pushSrv.registrarFirebase();
              this.app.getActiveNav().setRoot(ContatosPage);
            } else {
              this.pushSrv.checkCordova();
            }
          }
        })
    });
  }
}

