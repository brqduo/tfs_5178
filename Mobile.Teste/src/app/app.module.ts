import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContatosPage } from '../pages/contatos/contatos';
import { ContatoPage } from '../pages/contato/contato';
import { FiltroPage } from '../pages/filtro/filtro';

import { ModalImageComponent } from '../components/modal-image/modal-image';

import {
  OnsPackage,
  LoginService,
  ErrorService,
  LoginPage,
  UserService,
  StorageService,
  ImageService,
  // SecurityService,
  NetWorkService,
  UtilService
} from '@ons/ons-mobile-login'

import {
  AnalyticsService,
  OnsAnalyticsModule
} from '@ons/ons-mobile-analytics'
import { HttpClientModule } from '@angular/common/http';
import { ContatoService } from '../services/contatos.service';
//import { UtilService } from './../services/util.service';

import { PushService } from '../services/push.service';
import { Firebase } from '@ionic-native/firebase';
import { Device } from '@ionic-native/device';
import { AppVersion } from '@ionic-native/app-version';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ContatosPage,
    ContatoPage,
    FiltroPage,
    ModalImageComponent
  ],
  imports: [
    BrowserModule,
    OnsPackage.forRoot(),
    HttpClientModule,
    OnsAnalyticsModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Voltar'
    }),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ContatosPage,
    ContatoPage,
    FiltroPage,
    LoginPage,
    ModalImageComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ContatoService,
    // AnalyticsPackage
    AnalyticsService,
    // LoginPackage
    UtilService,
    LoginService,
    ErrorService,
    UserService,
    StorageService,
    ImageService,
    // SecurityService,
    NetWorkService,
    // Firebase
    Device,
    PushService,
    AppVersion,
    Firebase,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
