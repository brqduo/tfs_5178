import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core"; //, NgZone, Input
import { Content } from 'ionic-angular';
import { ViewController, Platform } from 'ionic-angular';
import { Gerencia } from '../../models/gerencia';
import { ContatoService } from '../../services/contatos.service';
//import { UtilService } from './../../services/util.service';
import { UtilService } from '@ons/ons-mobile-login';

import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/interval";
import "rxjs/add/operator/take";
import "rxjs/add/operator/map";
import "rxjs/add/operator/bufferCount"

import { Searchbar } from 'ionic-angular';

import { ElementRef } from '@angular/core';
// import { Nav } from 'ionic-angular';


@Component({
    selector: 'page-filtro',
    templateUrl: 'filtro.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FiltroPage {


    @ViewChild(Content) content: Content;
    @ViewChild('filtro_bar') objInput: ElementRef;
    @ViewChild('filtro_bar') objInputBar: Searchbar;
    @Input()
    public gerencias: Gerencia[] = [];
    private gerenciasCache: Gerencia[];
    public filtro: string;


    obsFiltro: any; //= Observable.fromEvent(this.objInput, 'input')
    public that = this;




    constructor(
        private viewController: ViewController,
        public platform: Platform,
        private changeDetector: ChangeDetectorRef,
        private service: ContatoService,
        private elementRef: ElementRef,
        public utilSrv: UtilService //,
        // public nav: Nav
    ) {
        //  console.log('entrei em filtro', this.objInput)
        this.obsFiltro = Observable.fromEvent(this.elementRef.nativeElement, 'input')

        this.carregarGerencias();

    }

    ionViewDidLoad() {
        var that: any = this;
        this.obsFiltro
            .map(event => event.target.value)
            .debounceTime(500)
            .distinctUntilChanged()
            .subscribe({
                next: function(value) {
                    // console.log(value);
                    that.utilSrv.filtroAtivo = value;
                    that.getItems(value)
                }
            });

        this.utilSrv.ObservableFecharGerencias
            .subscribe(data => {
                // console.log('teste de exit do filtro');

                var u: UIEvent;
                this.objInputBar.clearInput(u)
                this.filtro = '';// this.LimparFiltro('');
            })
    }

    carregarGerencias() {
        this.service.initGerenciaDB()
            .then(() => {
                this.service.getGerencias()
                    .subscribe(data => {
                        this.gerencias = []
                        data.forEach((element: any) => {
                            if (element.ID !== '') {
                                this.gerencias.push(element)
                            }
                        });


                        this.changeDetector.markForCheck();
                    },
                        error => {
                            console.error.bind(console);
                        })
            })
    }

    getItems(ev: any) {
        if (!this.gerenciasCache || this.gerenciasCache.length == 0) {
            this.gerenciasCache = this.gerencias;
        }
        let val = ev + '';
        this.gerencias = this.gerenciasCache.filter((gerencia: any) => {
            return ((gerencia.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1) || (gerencia.GerenciaExecutiva.toLowerCase().indexOf(val.toLowerCase()) > -1) || (val == ''));
        });
        this.changeDetector.detectChanges();
    }

    filtrarGerencia(gerencia: any) {
        // console.log('gerencia no filtrar => ', gerencia)
        this.utilSrv.refreshContatos.next(gerencia)
        this.viewController.dismiss();
    }

    fechar() {
        this.viewController.dismiss();
    }

    LimparFiltro(ev) {
        if ((ev.target.value === '') || (ev.target.value === undefined) || (ev.target.value === null)) {
            this.carregarGerencias();
        }
    }
}
