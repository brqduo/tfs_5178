import { Config } from './../../environment/config';
import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { NavParams, ViewController, Content, NavController, Platform, ModalController } from 'ionic-angular';
import { ContatoService } from '../../services/contatos.service';
import { Contato } from '../../models/contato';
import { Gerencia } from '../../models/gerencia';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { ModalImageComponent } from '../../components/modal-image/modal-image';
import { NetWorkService } from '@ons/ons-mobile-login';
//import { Navbar } from 'ionic-angular/navigation/nav-interfaces';

@Component({
  selector: 'page-contato',
  templateUrl: 'contato.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class ContatoPage {

  @ViewChild(Content) content: Content;
  public contato: any = {};
  public contatosMesmaGerencia: Contato[] = [];
  public background = 'assets/img/background/background-8.jpg';
  public Interesses = [];

  constructor(
    private viewCtrl: ViewController,
    public platform: Platform,
    private navParams: NavParams,
    private service: ContatoService,
    private changeDetector: ChangeDetectorRef,
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    public networkSrv: NetWorkService,
    private contatoSrv: ContatoService,
    private analyticsService: AnalyticsService, ) {
      

  }

  ionViewDidLoad() {    
    let c = this.navParams.get('contato');
    if (c) {
      this.contato = c;
      this.carregarContatosMesmaGerencia();
    } else {
      this.dismiss();
    }


    
    this.Interesses = this.contato.Interesse.split(Config.delimiterInteresses);

    this.platform.ready().then(() => {
      this.networkSrv.updateNetworkStatus();

      console.log(this.networkSrv.isNetworkConnected());
     })
  }

  dismiss() {
    this.viewCtrl.dismiss(this.contato);
  }

  carregarContatosMesmaGerencia() {
    this.service.initContatosDB()
      .then(() => {
        if (this.contato.Gerencia) {
          let gerencia: Gerencia = { ID: "", Nome: this.contato.Gerencia, GerenciaExecutiva: "", GerenciaCount: 0 };

          this.service.getContatosPorGerencia(gerencia)
            .then(data => {
              this.contatosMesmaGerencia = data.filter((contato) => {
                return contato.Nome.toLowerCase().indexOf(this.contato.Nome.toLowerCase()) == -1;
              });
              this.changeDetector.markForCheck();
            })
            .catch(console.error.bind(console));
        } else {
          this.contatosMesmaGerencia = [];
        }
        this.content.resize();
      });
  }

  enviarEmail(contato: Contato) {
    this.platform.ready().then(() => {
      if (contato.Email) {
        this.analyticsService.sendCustomEvent("Enviar Email", {
          "Email": contato.Email
        });
        window.location.href = 'mailto:' + contato.Email;
      }
    });
  }

  ligar(contato: Contato) {
    this.platform.ready().then(() => {
      if (contato.Telefone) {
        this.analyticsService.sendCustomEvent("Ligar", {
          "Telefone": contato.Telefone
        });
        window.location.href = 'tel:' + contato.Telefone;
      }
    });
  }

  whatsapp(contato: Contato) {
    this.platform.ready().then(() => {
      if (contato.Celular) {
        this.analyticsService.sendCustomEvent("Ligar", {
          "Celular": contato.Celular
        });
        // window.open('tel:' + contato.Celular, '_system');
        window.location.href = 'tel:' + contato.Celular;
      }
    });
  }

  showContato(contato: Contato) {
    this.analyticsService.sendContentView(
      contato.Nome,
      "Contato Mesma Gerência",
      contato._id,
      {
        "Cargo": contato.Cargo,
        "Gerência": contato.Gerencia,
        "Gerência Executiva": contato.GerenciaExecutiva,
        "Localização": contato.Localizacao
      }
    );

    this.navCtrl.push(ContatoPage, {
      contato: contato
    });
  }

  
  abrirImagemPerfil(id){
    let IsConnected = false;
    
    this.platform
    .ready()
    .then(() =>{

        IsConnected = this.networkSrv.isNetworkConnected().connected;

        if(IsConnected){

          let imgPerfilModal = this.modalCtrl.create(ModalImageComponent,{id:id},{cssClass:'select-modal'});
          imgPerfilModal.present(); 
        }
        else {
          let imgPerfilModal = this.modalCtrl.create(ModalImageComponent, { id: id, notConnected: Config.imgNotConnected }, { cssClass: 'select-modal' });
          imgPerfilModal.present();
        }

    })
    .catch(error =>{
      console.log(error);
    });
  }
    
}
