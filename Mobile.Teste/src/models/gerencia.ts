export interface Gerencia {
    ID: string;
    Nome: string;
    GerenciaExecutiva: string;
    GerenciaCount: number;
}
