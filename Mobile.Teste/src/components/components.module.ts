import { NgModule } from '@angular/core';
import { ModalImageComponent } from './modal-image/modal-image';
import { Nav } from 'ionic-angular';

@NgModule({
	declarations: [ModalImageComponent],
	imports: [],
	exports: [ModalImageComponent],
	providers:[Nav]
})

export class ComponentsModule {}
