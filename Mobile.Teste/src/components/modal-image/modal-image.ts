import { Component, ViewChild } from '@angular/core';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ViewController, Navbar, Platform } from 'ionic-angular';
import { ContatoService } from '../../services/contatos.service';
import { Config } from './../../environment/config';

/**
 * Generated class for the ModalImageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'modal-image',
  templateUrl: 'modal-image.html'
})
export class ModalImageComponent {
  @ViewChild(Navbar) navBar: Navbar;
  id: string;
  url_src: string;
  loading = false;
  notConnected = false;

  constructor(private Params: NavParams,
    private contatoSrv: ContatoService,
    private platform: Platform,
    // public nav: Nav,
    // private navbarCtrl: NavController,
    private viewCtrl: ViewController) {

    this.id = Params.get('id');
    this.notConnected = Params.get('notConnected') || false;

    // this.platform.registerBackButtonAction(() => {
    //   this.closeModal();
    // },1);

    this.getImagemDoPerfil();
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      this.viewCtrl.dismiss();
    }
  }

  getImagemDoPerfil() {
    this.loading = true;

    if (this.notConnected) {
      this.url_src = Config.imgNotConnected;
      this.loading = false;
    }
    else {
      this.contatoSrv.getImagemPerfil(this.id).then((result: any) => {
        this.loading = false;

        if (result == [] || result == null || result.length == 0 || result == "[]") {
          this.url_src = Config.imgNotFound;
        }
        else {
          let obj = JSON.parse(result);
          this.url_src = obj[0].LargeImage;
        }
      }).catch(error => {
        console.log(error);
        this.loading = false;
      });
    }
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
  closeModalContent() {
    this.viewCtrl.dismiss();
  }
}
