'use strict';
/* const AWS = require('aws-sdk'); */
var admin = require('firebase-admin');
exports.handler = (event, context, callback) => {
    console.log(JSON.stringify(`Event: event`));
    context.callbackWaitsForEmptyEventLoop = false;
    const serviceAccount = {
        "type": "service_account",
        "project_id": "brq-push-test",
        "private_key_id": "46e20330e46b85232881677303c17fc9b86aba97",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCnb+OLIVQfqWMP\n0ytAk05R3sCd/YrhcUQLC6lgHC4wlQ+mCX0qH2KV0QSaJo3znyRy5aAuexpUJgny\n+I2N+HutPhJ6tECrpaBpcxvFLQJU0YaAhbxypXrHqxD78GrFoleRpy3HyztIucCJ\nyahtUsbJFZpiJTMw8Tu6Vmt+nHTW++QAqMPh9w58flawxeyY3GEsI+jLokWj0QrO\n/19RPwtDUpa05H7gyjubcVqSBg43JZgfZsQhyZTGcCiPi3bpZKkrxqynaS2PZboX\n9uw+wsT59y7ehl1yXj7nlZnQ0KZTGXVR6k4hbVw9jCY0KE+Js0StLgTyyBtvLfTQ\nSOhOX/ARAgMBAAECggEAQt8ym0oj3YNaz6KwpxlOEe+rsU3mblS87sZpDrwijH+0\nIMWwaibfymMQ+foE/rRkK8lfGMFMyifpGFPBwV5pEs90NPKR05wSPZteD3mC8Vo/\nZExFb+p8Z4uAfIbQNf+5BrCk3aNjGZAqgD18mbESOJR6tNETqWvnRWwngYk+Nu/8\n+eKGppjV4SKNujAC9xyTljUi2S1xnjXEKxQYeKj/FHh5GWdUXwx10448Ch6QJhLx\nD+vRhFQIb4bCGDcLi8N8RvrfPsB7Kpsasdf7VPgMYy39K/i/2qMfsvV5YhNXmTop\ngBXTVczEDcfwJ0GaHo/+/pvyubAaWgil3HufoFqzywKBgQDUQZQPhbziojrqUhGm\nX1x7eO2vBTP/KlqFUdWpISi+QAo/1pQCYQTlclTzwHZ12GYGSpk7XLtJfLRBAQN5\nB00MrwuwoaM6Pxnu3/kRR3V6wBBWr8wd2NfzBkOXDqj01CSWP/zr/jTaOV5jJ1Zv\nFx9nC70OFrVLifN8jhfbUiPUTwKBgQDJ8bMsRKREAD8kC6uSs72shnk8fvTx9aWy\n7QFhmTzabzhDriVlRYZP6mNPNboeRNQ+i/Gxg5Cmq/Fv3PWzuq0sqX6NqbZDZ8lF\nG920f+/UKxSg5MhiCvqPF01cl4jF4zqKZdEBh4OVorTNVRcmAARwWhkPBWXX5kFe\n9cHOXaP9nwKBgQCAfw7ecs1Nun7GOlbbDKIVUbk6moDG+eIBEfCyShO7UVOjIAlM\ng2WObi/FvP86Cn6BUon/aP0s2OTa8X0juOrJQfNmxAPoECln9ol3D3askDeT8ysz\n6oGZa3JhsYtRZFid6TD5wVRHxDFC/56Q9EObg3y5Wb/5chNIlVEWAl0UewKBgG6l\nY4ppwacQi4fwbP1gRhh1RZAD76ly8mrquMsE7vav4aATlqG6SjEXN3p68ZMMG055\n1t9wOp1XsF2eL3FeYuwJojF0P00HXJvk/MCdKIaTZCYTtEcjxM0J83U8AdCLA9Q4\nnR+9ZDdkgrPOX3SOpd/5PamhR3/XxRGGjb3ISMX5AoGBALLqnKee/4gJBKKw2/VG\nLvVn5OWZOw6IhBk7fc5k5Q9LJgzunHnJTTkYddj3s4o+hek3nfkWhvP/dB/Vq+Gx\nY1fRKFCjPYVPUlflikXW1oMJVMbR1N/eREJ6EB/YOxgLhXvi28S8Ba2JFX5C+6y/\neQ9woZceD/Ben2mZghznFgfO\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-lfptw@brq-push-test.iam.gserviceaccount.com",
        "client_id": "109499788075326336983",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-lfptw%40brq-push-test.iam.gserviceaccount.com"
    }

    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://brq-push-test.firebaseio.com"
    });

    const registrationtoken = event.token;
    const colorHexFull = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);

    if (event.color === undefined || event.color === null) {
        event.color = colorHexFull;
    }

    if (event.data === undefined || event.data === null) {
        event.data = {
            resultado: 'Notificação Enviada, mas sem parametros especiais'
        };
    };
    const payload = {
        data: event.data,
        notification: {
            body: event.body,
            tap: "false",
            color: event.color,
            title: event.title,
        }
    }
    const options = {
        priority: 'high',
        timeToLive: 5000
    };

    admin.messaging().sendToDevice(registrationtoken, payload, options)
        .then((response) => {
            const Result = {
                res: 'Push Enviado com sucesso',
                data: response
            }
            resultado(Result);
        }).catch((err) => {
            const Result = {
                res: 'Erro no envio do Push',
                data: err
            }
            error(Result);
        })


    function resultado(res) {
        console.log(res);
        
        callback(null, res);
    };

    function error(err) {
        console.log(err);
        
        callback(err, null);
    }
}