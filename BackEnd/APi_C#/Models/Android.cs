﻿using Newtonsoft.Json;
using System;

namespace API.Mobile.PushNotifications.Models
{
    [Serializable]
    public class Android
    {
        [JsonProperty("nome")]
        public string Nome { get; set; }

        [JsonProperty("acao")]
        public string Acao { get; set; }
    }
}