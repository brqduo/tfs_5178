﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace API.Mobile.PushNotifications.Models
{
    [Serializable]
    public class ParametroNotificacao
    {
        [JsonProperty("origem")]
        public string Origem { get; set; }

        [JsonProperty("texto")]
        public string Texto { get; set; }

        [JsonProperty("titulo")]
        public string Titulo { get; set; }

        [JsonProperty("prioridade")]
        public string Prioridade { get; set; }

        [JsonProperty("som")]
        public string Som { get; set; }

        [JsonProperty("indicadorIOS")]
        public string IndicadorIOS { get; set; }

        [JsonProperty("TipoMsg")]
        public string TipoMSG { get; set; }

        [JsonProperty("dataEnvio")]
        public string DataEnvio { get; set; }

        [JsonProperty("audiencia")]
        public List<Audiencia> Audiencia { get; set; }

        [JsonProperty("acoes")]
        public List<Acoes> Acoes { get; set; }

    }
}