﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace API.Mobile.PushNotifications.Models
{
    [Serializable]
    public class Audiencia
    {
        [JsonProperty("topicos")]
        public List<string> Topicos { get; set; }

        [JsonProperty("usuarios")]
        public List<string> Usuarios { get; set; }

        [JsonProperty("dispositivos")]
        public List<string> Dispositivos { get; set; }
    }
}