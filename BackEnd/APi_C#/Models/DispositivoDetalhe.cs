﻿using Newtonsoft.Json;
using System;

namespace API.Mobile.PushNotifications.Models
{
    public class DispositivoDetalhe
    {
        [JsonProperty("so")]
        public string SO { get; set; }

        [JsonProperty("modelo")]
        public string Modelo { get; set; }
    }
}