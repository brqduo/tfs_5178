﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace API.Mobile.PushNotifications.Models
{
    [Serializable]
    public class ParametroRegistro
    {
        [JsonProperty("usuarioId")]
        public string UsuarioId { get; set; }

        [JsonProperty("appName")]
        public string AppName { get; set; }

        [JsonProperty("appVersao")]
        public string AppVersao { get; set; }

        [JsonProperty("dispositivoId")]
        public string DispositivoId { get; set; }

        [JsonProperty("dispositivoDetalhe")]
        public DispositivoDetalhe DispositivoDetalhe { get; set; }

        [JsonProperty("topicos")]
        public List<string> Topicos { get; set; }
    }
}