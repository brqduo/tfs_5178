﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Mobile.Contatos.Models
{
    
    public class Usuario
    {
        [JsonProperty("usuarioId")]
        public string UsuarioId { get; set; }

        [JsonProperty("dispositivos")]
        public List<Dispositivo> Dispositivos { get; set; }
    }
}