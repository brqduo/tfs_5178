﻿using API.Mobile.PushNotifications.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace API.Mobile.Contatos.Models
{
    
    public class Dispositivo
    {
        [JsonProperty("appName")]
        public string AppName { get; set; }

        [JsonProperty("appVersao")]
        public string AppVersao { get; set; }

        [JsonProperty("dispositivoDetalhe")]
        public DispositivoDetalhe DispositivoDetalhe { get; set; }

        [JsonProperty("dispositivoId")]
        public string DispositivoId { get; set; }

        [JsonProperty("topicos")]
        public List<string> Topicos { get; set; }
    }
}