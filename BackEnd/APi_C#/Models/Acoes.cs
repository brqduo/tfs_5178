﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace API.Mobile.PushNotifications.Models
{
    [Serializable]
    public class Acoes
    {
        [JsonProperty("Android")]
        public List<Android> Android { get; set; }

        [JsonProperty("IOS")]
        public List<IOS> IOS { get; set; }
    }
}