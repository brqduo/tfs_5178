﻿using Newtonsoft.Json;
using System;

namespace API.Mobile.PushNotifications.Models
{
    [Serializable]
    public class IOS
    {
        [JsonProperty("nome")]
        public string Nome { get; set; }

        [JsonProperty("categoria")]
        public string Categoria { get; set; }
    }
}