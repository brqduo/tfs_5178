﻿using RestSharp;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;
using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace API.Mobile.PushNotifications.Util
{
    public static class DoRequest
    {
        public static Util_service utilSrv = new Util_service();

        private static RestClient client = new RestClient(ConfigurationManager.AppSettings.Get("urlrest"));

        private static ILog log = LogManager.GetLogger(typeof(DoRequest));
        
        public static async Task<string> ExecutePOSTJsonHttpClient(string url, object body)
        {
            var returnedString = await Task.Run(() => MainAsync(url, body));
            return returnedString;
        }

        public static async Task<String> MainAsync(string url, object body)
        {
            using (HttpClient client = new HttpClient(new HttpClientHandler() { Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint")) }))
            {
                client.BaseAddress = new System.Uri(ConfigurationManager.AppSettings.Get("urlrest"));
                client.DefaultRequestHeaders.Add("Accept", "application/json; odata=verbose");
                string digest = GetRequestDigest().FormDigestValue.ToString();
                Console.WriteLine("Digest " + digest);
                try
                {
                    var ret = await PerformPOST(client, digest, url, body);
                    return ret;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    return ex.Message;
                }
            }
        }

        private static async Task<String> PerformPOST(HttpClient client, string digest, string url, object body)
        {
            client.DefaultRequestHeaders.Add("X-RequestDigest", digest);
            var request = body;
            string json = JsonConvert.SerializeObject(request);
            StringContent strContent = new StringContent(json);
            strContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json;odata=verbose");
            HttpResponseMessage response = await client.PostAsync(url, strContent);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
                return content;
            }
            else
            {
                Console.WriteLine(response.StatusCode);
                Console.WriteLine(response.ReasonPhrase);
                string content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
                return content;
            }
        }

        private static XmlMapper.ContextWebInformation GetRequestDigest()
        {
            log4net.Config.XmlConfigurator.Configure();

            var request = new RestRequest("/_api/contextinfo", Method.POST)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"))
            };
            request.RequestFormat = DataFormat.Xml;
            request.XmlSerializer = new RestSharp.Serializers.DotNetXmlSerializer();
            XmlMapper.ContextWebInformation response = Execute<XmlMapper.ContextWebInformation>(request);

            return response;
        }

        public static T GetToSharepoint<T>(string url) where T : new()
        {
            var request = new RestRequest(url, Method.GET);
            request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"));
            request.RequestFormat = DataFormat.Xml;
            request.XmlSerializer = new RestSharp.Serializers.DotNetXmlSerializer();
            //request.RootElement = "feed";
            T response = Execute<T>(request);
            return response;
        }

        public static HttpWebRequest GetRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Accept = "application/atom+xml";
            request.ContentType = "application/atom+xml;type=entry";

            request.Credentials = utilSrv.GetSharepointCredential();
            return request;
        }

        public static dynamic GetToSharepointJson<T>(string url) where T : new()
        {
            var request = new RestRequest(url, Method.GET);
            request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"));
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Accept","application/json;odata=verbose");


            request.JsonSerializer = new RestSharp.Serializers.JsonSerializer();
            var result = Execute<dynamic>(request);
            return result; // DeserializeToList<T>(result);
        }

        public async static Task<T> GetToSharepointAsync<T>(string url) where T : new()
        {
            var request = new RestRequest(url, Method.GET);
            request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"));
            request.RequestFormat = DataFormat.Xml;
            request.XmlSerializer = new RestSharp.Serializers.DotNetXmlSerializer();
            //request.RootElement = "feed";
            T response = await ExecuteAsync<T>(request);
            return response;
        }

        public static dynamic GetProfileImage(string username)
        {
            var request = new RestRequest("_layouts/15/gera_thumbnail.aspx?url=" + ConfigurationManager.AppSettings.Get("urlrest") + "/Import%20Profile/" + username + ".jpg&w=80&h=80", Method.GET);
            request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"));
            request.RequestFormat = DataFormat.Json;

            //request.RootElement = "feed";
            dynamic response = client.Execute(request);
            return GetImageBase64(response.bytes);
        }

        public static T Execute<T>(RestRequest request) where T : new()
        {
            var response = client.Execute<T>(request);
            return response.Data;
        }

        public static List<XElement> ExecuteXML(HttpWebRequest request)
        {
            WebResponse response = request.GetResponse();
            var s = new XmlDocument();
            XDocument oDataXML = XDocument.Load(response.GetResponseStream(), LoadOptions.None);
            s.LoadXml(oDataXML.ToString());

            XNamespace d = "http://schemas.microsoft.com/ado/2007/08/dataservices";
            
            List<XElement> items = oDataXML.Descendants(d + "query")
                                     .Elements(d + "PrimaryQueryResult")
                                     .Elements(d + "RelevantResults")
                                     .Elements(d + "Table")
                                     .Elements(d + "Rows")
                                     .Elements(d + "element")
                                     .ToList();
            return items;
        }


        private static string GetSharepointUrl(int pagina)
        {
            string filtro = "*";
            string refinementfilters = @"&refinementfilters=%27OR(Gerencia-Executiva:(%22a*%22)%2cGerencia-Executiva:(%22b*%22)%2cGerencia-Executiva:(%22c*%22)%2cGerencia-Executiva:(%22d*%22)%2cGerencia-Executiva:(%22e*%22)%2cGerencia-Executiva:(%22f*%22)%2cGerencia-Executiva:(%22g*%22)%2cGerencia-Executiva:(%22h*%22)%2cGerencia-Executiva:(%22i*%22)%2cGerencia-Executiva:(%22j*%22)%2cGerencia-Executiva:(%22k*%22)%2cGerencia-Executiva:(%22l*%22)%2cGerencia-Executiva:(%22m*%22)%2cGerencia-Executiva:(%22n*%22)%2cGerencia-Executiva:(%22o*%22)%2cGerencia-Executiva:(%22p*%22)%2cGerencia-Executiva:(%22q*%22)%2cGerencia-Executiva:(%22r*%22)%2cGerencia-Executiva:(%22s*%22)%2cGerencia-Executiva:(%22t*%22)%2cGerencia-Executiva:(%22u*%22)%2cGerencia-Executiva:(%22v*%22)%2cGerencia-Executiva:(%22w*%22)%2cGerencia-Executiva:(%22x*%22)%2cGerencia-Executiva:(%22y*%22)%2cGerencia-Executiva:(%22z*%22)%2cGerencia-Executiva:(%221*%22)%2cGerencia-Executiva:(%222*%22)%2cGerencia-Executiva:(%223*%22)%2cGerencia-Executiva:(%224*%22)%2cGerencia-Executiva:(%225*%22)%2cGerencia-Executiva:(%226*%22)%2cGerencia-Executiva:(%227*%22)%2cGerencia-Executiva:(%228*%22)%2cGerencia-Executiva:(%229*%22)%2cGerencia-Executiva:(%220*%22))%27";
            //http://intranet.ons.org.br/_api/search/query?querytext=%27anderson%27&trimduplicates=false&rowsperpage=500&rowlimit=500&sourceid=%27b09a7990-05ea-4af9-81ef-edfab16c4e31%27&selectproperties=%27UserName,Title,JobTitle,WorkEmail,WorkPhone,Andar,Gerencia-Executiva,Gerencia,Matricula,MobilePhone%27
            //string colunas = @"&selectproperties=%27PreferredName%2cAccountName%2cDataNascimento%2cPhoneNumber%2cUserName%2cTitle%2cJobTitle%2cWorkEmail%2cWorkPhone%2cAndar%2cGerencia-Executiva%2cGerencia%2cMatricula%2cRamal%2cPictureUrl%27";
            //string colunas = @"&selectproperties=%27UserName,Title,JobTitle,WorkEmail,WorkPhone,Andar,Gerencia-Executiva,Gerencia,Matricula,MobilePhone,PreferredName,AccountName,DataNascimento,PictureURL,Matricula%27";
            string colunas = @"&selectproperties=%27UserName,Gerencia%27";
            string url = string.Format(@"/_api/search/query?querytext=%27{0}%27&trimduplicates=false&StartRow={1}&rowlimit={2}&sourceid=%27{3}%27{4}{5}"
                                       , filtro, pagina, 5000, ConfigurationManager.AppSettings.Get("SourceID"), colunas, refinementfilters);

            return url;
        }

        public async static Task<T> ExecuteAsync<T>(RestRequest request) where T : new()
        {
            var response = await client.ExecuteTaskAsync<T>(request);

            return response.Data;
        }

        private static string GetImageBase64(byte[] data)
        {
            return "data:image/jpg;base64," + Convert.ToBase64String(data);
        }

        public async static Task<Stream> GetAttachmentStreamAsync(string url)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"));
                return await wc.OpenReadTaskAsync(url);
            }
        }

        public static List<string> InvalidJsonElements;

        public static IList<T> DeserializeToList<T>(string jsonString)
        {
            InvalidJsonElements = null;
            var array = JArray.Parse(jsonString);
            IList<T> objectsList = new List<T>();

            foreach (var item in array)
            {
                try
                {
                    // CorrectElements
                    objectsList.Add(item.ToObject<T>());
                }
                catch (Exception ex)
                {
                    InvalidJsonElements = InvalidJsonElements ?? new List<string>();
                    InvalidJsonElements.Add(item.ToString());
                    Console.WriteLine(InvalidJsonElements.ToString());
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    throw (ex);
                }
            }

            return objectsList;
        }

    }
}