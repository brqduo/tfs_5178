﻿using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Caching.Backends;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace API.Mobile.PushNotifications.Util
{
    public static class UserService
    {

        public static void InitializeCache()
        {
            CachingServices.DefaultBackend = new MemoryCachingBackend();
        }

        public static void UpdateAll()
        {
            CachingServices.DefaultBackend.Clear();
        }
        
        [Cache(AbsoluteExpiration = 86400)]
        public static string GetImageBase64(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            else
            {
                byte[] data = null;
                string formato = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    try
                    {
                        using (WebClient client = new WebClient())
                        {
                            client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"));
                            try
                            {
                                data = client.DownloadData(new Uri(path));
                            }
                            catch (WebException e)
                            {
                                return e.Message;
                            }
                            formato = Path.GetExtension(path).Substring(1);

                            return "data:image/" + formato + ";base64," + Convert.ToBase64String(data);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        [Cache(AbsoluteExpiration = 86400)]
        public async static Task<string> GetImageBase64Async(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            else
            {
                byte[] data = null;
                string formato = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    try
                    {
                        using (WebClient client = new WebClient())
                        {
                            client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("usuarioSharePoint"), ConfigurationManager.AppSettings.Get("senhaSharePoint"));
                            try
                            {
                                data = await client.DownloadDataTaskAsync(new Uri(path));
                            }
                            catch (WebException e)
                            {
                                return e.Message;
                            }
                            formato = Path.GetExtension(path).Substring(1);

                            return "data:image/" + formato + ";base64," + Convert.ToBase64String(data);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}