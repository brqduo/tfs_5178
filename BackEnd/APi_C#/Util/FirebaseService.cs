﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using API.Mobile.Contatos.Firebase;
using API.Mobile.Contatos.Models;
using API.Mobile.PushNotifications.Models;
using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;
using Newtonsoft.Json;

namespace API.Mobile.PushNotifications.Util
{
    public class FirebaseService
    {
        private static int ContatosIntranetServicePageSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("ContatosIntranetServicePageSize"));
        private static string _firebaseCloudMessagingURL = ConfigurationManager.AppSettings["FirebaseCloudMessagingURL"];
        private static string _firebaseDatabaseURL = ConfigurationManager.AppSettings["FirebaseDatabaseURL"];
        private static string _serverKey = ConfigurationManager.AppSettings["ServerKey"];
        private static string _senderId = ConfigurationManager.AppSettings["SenderId"];
        private static Util_service util_Service = new Util_service();
        private static IFirebaseConfig _firebaseConfig = new FirebaseConfig
        {
            AuthSecret = "GVsnDEoQiyu0E9KMpzUkoCFplDCLU2h7bTZH6aqH",
            BasePath = _firebaseDatabaseURL
        };

        public RespostaApi Registrar()
        {
            try
            {
                HttpRequest request = HttpContext.Current.Request;
                FirebaseDB firebaseDB = new FirebaseDB(_firebaseDatabaseURL);
                JwtSecurityToken token = RetornarToken(request.Headers["Authorization"]);
                ParametroRegistro parametroRegistro = JsonConvert.DeserializeObject<ParametroRegistro>(request.Form[0]);
                                  parametroRegistro.AppName = RetornarValorClaims(token, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/authorizationdecision");


                var response = firebaseDB.Post(RetornarJsonRegistro(token, parametroRegistro));

                return RetornoObjetoApi(response);

            }
            catch (Exception ex)
            {
                RespostaApi retorno = new RespostaApi
                {
                    codigo = 400,
                    mensagem = "Erro ao executar a operação."
                };

                return retorno;
            }
        }

        public RespostaApi Notificar()
        {
            try
            {
                HttpRequest request = HttpContext.Current.Request;
                ParametroNotificacao parametroNotificacao = JsonConvert.DeserializeObject<ParametroNotificacao>(request.Form[0].Replace(@"\", @"\\"));
                JwtSecurityToken token = RetornarToken(request.Headers["Authorization"]);

                List<Usuario> listaUsuario = ObterUsuariosFirebase();
                List<string> ListaDispositivoID = new List<string>();

                List<string> Topicos = new List<string>();
                List<string> Usuarios = new List<string>();
                List<string> Dispositivos = new List<string>();

                foreach (Audiencia audiencia in parametroNotificacao.Audiencia)
                {
                    Topicos.AddRange(audiencia.Topicos);
                    Usuarios.AddRange(audiencia.Usuarios);
                    Dispositivos.AddRange(audiencia.Dispositivos);
                }

                if (Topicos.Any())
                {
                    ListaDispositivoID.AddRange(FiltrarUsuariosPorTopico(parametroNotificacao, listaUsuario, Topicos));
                }

                if (Usuarios.Any())
                {
                    ListaDispositivoID.AddRange(FiltrarUsuariosPorTopico(parametroNotificacao, listaUsuario, Usuarios));
                }

                if (Dispositivos.Any())
                {
                    ListaDispositivoID.AddRange(FiltrarUsuariosPorTopico(parametroNotificacao, listaUsuario, Dispositivos));
                }


                List<string> retorno = new List<string>();
                foreach (string dispositivoId in ListaDispositivoID.Distinct().ToList())
                {
                    retorno.Add(EnviarNotificacao(parametroNotificacao, dispositivoId));
                }

                var result = EnviarNotificacaoMultiplo(parametroNotificacao, ListaDispositivoID);

                return result;
            }
            catch (Exception ex)
            {
                RespostaApi retorno = new RespostaApi
                {
                    codigo = 400,
                    mensagem = "Erro ao executar a operação."
                };

                return retorno;
            }
        }

        public string[] getTopicosByApp(string nomeApp)
        {
            try
            { 
                string[] topicos = { };

                List<App> lista = new List<App>();

                if (nomeApp == null || nomeApp.Trim().Length == 0)
                {
                    return topicos;
                }

                lista = getAppList();

                foreach (var app in lista)
                {
                    if(app.nome == nomeApp)
                    {
                        topicos = app.topicos;
                    }
                }

                return topicos;
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
        }

        public List<Usuario> GetUsuariosByApp(string nomeApp)
        {
            try
            {
                List<Usuario> listaResultado = new List<Usuario>();

                List<Usuario> lista = new List<Usuario>();

                if (nomeApp == null || nomeApp.Trim().Length == 0)
                {
                    return lista;
                }

                lista = GetUsuarios();

                foreach (var usuario in lista)
                {
                    foreach (var usuarioDispositivos in usuario.Dispositivos)
                    {
                        if(usuarioDispositivos.AppName == nomeApp)
                        {
                            listaResultado.Add(usuario);
                        }
                    }
                }

                listaResultado = listaResultado.Distinct().ToList<Usuario>();
                return listaResultado;
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
        }

        public List<Usuario> GetUsuarios()
        {
            try
            {
                List<Usuario> lista = new List<Usuario>();
                IFirebaseClient client = new FirebaseClient(_firebaseConfig);
                FireSharp.Response.FirebaseResponse response = client.Get("");

                var listaJson = JsonConvert.DeserializeObject(response.Body);
                foreach (var item in ((Newtonsoft.Json.Linq.JObject)listaJson))
                {
                    if (item.Key == "Usuarios")
                    {
                        foreach (var usuario in ((Newtonsoft.Json.Linq.JArray)item.Value).Children())
                        {
                            lista.Add(JsonConvert.DeserializeObject<Usuario>(usuario.ToString()));
                        }
                    }
                }
                return lista;
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
        }

        public List<Usuario> GetUsuariosByTopicos(string[] topicos)
        {
            try
            { 
                List<Usuario> listaResultado = new List<Usuario>();

                List<Usuario> lista = new List<Usuario>();

                if (topicos == null || topicos.Length == 0)
                {
                    return lista;
                }

                lista = GetUsuarios();

                foreach (var usuario in lista)
                {
                    foreach (var topicoUsuario in usuario.Dispositivos.SelectMany(y => y.Topicos))
                    {
                        if (topicos.Contains(topicoUsuario))
                        {
                            listaResultado.Add(usuario);
                        }
                    }
                }

                listaResultado = listaResultado.Distinct().ToList<Usuario>();
                return listaResultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<App> getAppList()
        {
            try
            {
                List<App> lista = new List<App>();
                IFirebaseClient client = new FirebaseClient(_firebaseConfig);
                FireSharp.Response.FirebaseResponse response = client.Get("");

                var listaJson = JsonConvert.DeserializeObject(response.Body);
                foreach (var item in ((Newtonsoft.Json.Linq.JObject)listaJson))
                {
                    if (item.Key == "Apps")
                    {
                        foreach (var app in (item.Value).Children())
                        {
                            if (app.ToString().StartsWith("{"))
                            {
                                lista.Add(JsonConvert.DeserializeObject<App>(app.ToString()));
                            }
                            else
                            {
                                lista.Add(JsonConvert.DeserializeObject<App>(app.First.ToString()));
                            }
                        }
                    }
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string addApp(App app)
        {
            try
            {
                List<App> AppExistentes = new List<App>();

                AppExistentes = getAppList();

                foreach (var appExistente in AppExistentes)
                {
                    if(appExistente.nome == app.nome)
                    {
                        return "Já existe um App cadastrado com o mesmo nome";
                    }
                }

                IFirebaseClient client = new FirebaseClient(_firebaseConfig);
                FireSharp.Response.PushResponse response = client.Push("Apps", app);

                return response.Result.name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<App> addAppList(List<App> listaApps)
        {
            try
            { 
                if( listaApps == null || listaApps.Count == 0)
                {
                    return new List<App>();
                }

                IFirebaseClient client = new FirebaseClient(_firebaseConfig);
                FireSharp.Response.SetResponse response = client.Set("Apps", listaApps);

                return response.ResultAs<List<App>>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private JwtSecurityToken RetornarToken(string authorization)
        {
            if (authorization.StartsWith("Bearer"))
            {
                authorization = authorization.Replace("Bearer", "").Trim();
            }

            JwtSecurityTokenHandler jwtHandler = new JwtSecurityTokenHandler();

            bool readableToken = jwtHandler.CanReadToken(authorization);
            JwtSecurityToken token = null;
            if (readableToken)
            {
                token = jwtHandler.ReadJwtToken(authorization);
            }

            return token;
        }

        private string RetornarValorClaims(JwtSecurityToken token, string chave)
        {
            return token.Claims.FirstOrDefault(x => x.Type == chave).Value.Replace(@"\",@"\\");
        }

        public string RetornarJsonRegistro(JwtSecurityToken token, ParametroRegistro parametroRegistro) {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            stringBuilder.Append("'usuarioId': '" + RetornarValorClaims(token, "unique_name") + "' ,");
            stringBuilder.Append("'dispositivos': ");
                stringBuilder.Append("[");
                    stringBuilder.Append(JsonConvert.SerializeObject(parametroRegistro));
                stringBuilder.Append("]");
            stringBuilder.Append("}");

            return stringBuilder.ToString();
        }

        public string EnviarNotificacao(ParametroNotificacao parametroNotificacao, string _deviceId)
        {
            WebRequest webRequest = WebRequest.Create(_firebaseCloudMessagingURL);
            webRequest.Method = "POST";
            webRequest.Headers.Add(string.Format("Authorization: key={0}", _serverKey));
            webRequest.Headers.Add(string.Format("Sender: id={0}", _senderId));
            webRequest.ContentType = "application/json";

           // string[] deviceIds = new string[] { "id1", "id2", "id3" };

            var data = new
            {
               // registration_ids = deviceIds,
                to = _deviceId,
                priority = parametroNotificacao.Prioridade,
                notification = new
                {
                    title = parametroNotificacao.Titulo,
                    body = parametroNotificacao.Texto,
                    sound = parametroNotificacao.Som,
                    show_in_foreground = "True"
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            webRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = webRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                WebResponse webResponse = webRequest.GetResponse();
                return ((System.Net.HttpWebResponse)webRequest.GetResponse()).StatusCode.ToString();
            }
        }

        public RespostaApi EnviarNotificacaoMultiplo(ParametroNotificacao parametroNotificacao, List<string> _deviceIds)
        {
            WebRequest webRequest = WebRequest.Create(_firebaseCloudMessagingURL);
            webRequest.Method = "POST";
            webRequest.Headers.Add(string.Format("Authorization: key={0}", _serverKey));
            webRequest.Headers.Add(string.Format("Sender: id={0}", _senderId));
            webRequest.ContentType = "application/json";

            // string[] deviceIds = new string[] { "id1", "id2", "id3" };

            var data = new
            {
                registration_ids = _deviceIds,
                priority = parametroNotificacao.Prioridade,
                tipoMSG = parametroNotificacao.TipoMSG,
                notification = new
                {
                    title = parametroNotificacao.Titulo,
                    body = parametroNotificacao.Texto,
                    sound = parametroNotificacao.Som,
                    show_in_foreground = "True"
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            webRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = webRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                WebResponse webResponse = webRequest.GetResponse();
                return RetornoObjetoApi((System.Net.HttpWebResponse)webRequest.GetResponse());
            }
        }

        private Usuario ObterUsuarioFirebase(string id)
        {
            IFirebaseClient client = new FirebaseClient(_firebaseConfig);
            FireSharp.Response.FirebaseResponse response = client.Get(id);
            Usuario usuario = response.ResultAs<Usuario>();
            return usuario;
        }

        private List<Usuario> ObterUsuariosFirebase()
        {
            List<Usuario> lista = new List<Usuario>();
            IFirebaseClient client = new FirebaseClient(_firebaseConfig);
            FireSharp.Response.FirebaseResponse response = client.Get("");

            var listaJson = JsonConvert.DeserializeObject(response.Body);
            foreach (var item in ((Newtonsoft.Json.Linq.JObject)listaJson))
            {
                lista.Add(JsonConvert.DeserializeObject<Usuario>(item.Value.ToString()));
            }
            return lista;
        }

        private List<string> FiltrarUsuariosPorTopico(ParametroNotificacao parametroNotificacao, List<Usuario> listaUsuario, List<string> listaTopicos) {

            List<string> retorno = new List<string>();
            foreach (string topico in listaTopicos.Distinct())
            {
                retorno.AddRange(
                    listaUsuario.Where(x => x.Dispositivos.First().Topicos.Contains(topico))
                                .Select(x => x.Dispositivos.First().DispositivoId)
                                .ToList()
                );
            }

            return retorno;
        }

        private List<string> FiltrarUsuariosPorUsuarioId(ParametroNotificacao parametroNotificacao, List<Usuario> listaUsuario, List<string> listaUsuarioId)
        {
            List<string> retorno = new List<string>();
            foreach (string usuario in listaUsuarioId.Distinct())
            {
                retorno.AddRange(
                    listaUsuario.Where(x => x.UsuarioId.ToLower().Equals(usuario.ToLower()))
                                .Select(x => x.Dispositivos.First().DispositivoId)
                                .ToList()
                );
            }

            return retorno;
        }

        private List<string> FiltrarUsuariosPorDispositivo(ParametroNotificacao parametroNotificacao, List<Usuario> listaUsuario, List<string> listaDispositivos)
        {
            List<string> retorno = new List<string>();
            foreach (string dispositivo in listaDispositivos.Distinct())
            {
                retorno.AddRange(
                    listaUsuario.Where(x => x.Dispositivos.First().DispositivoId.ToLower().Equals(dispositivo.ToLower()))
                                .Select(x => x.Dispositivos.First().DispositivoId)
                                .ToList()
                );
            }

            return retorno;
        }

        private RespostaApi RetornoObjetoApi(FirebaseResponse FireResponse)
        {
            var msg = "Ok";
            var code = 200;

            var status = FireResponse.HttpResponse.StatusCode;
            

            switch (status)
            {
                case HttpStatusCode.OK:
                    msg = "Operação realizada com sucesso.";
                    code = 200;
                    break;
                case HttpStatusCode.BadRequest:
                    msg = "Erro ao executar a operação.";
                    code = 400;
                    break;
                case HttpStatusCode.Unauthorized:
                    msg = "Erro de autorização. Verifique se o usuário solicitante tem as permissões necessárias.";
                    code = 401;
                    break;
                case HttpStatusCode.NotFound:
                    msg = "O recurso requisitado não foi encontrado.";
                    code = 404;
                    break;
                case HttpStatusCode.InternalServerError:
                    msg = "Erro de execução no servidor.";
                    code = 500;
                    break;
                default:
                    break;
            }

            RespostaApi retorno = new RespostaApi
            {
                codigo = code,
                mensagem = msg
            };

            return retorno;

        }


        private RespostaApi RetornoObjetoApi(HttpWebResponse WebResponse)
        {
            var msg = "Ok";
            var code = 200;

            var webResponseStream = WebResponse.GetResponseStream();

            var status = WebResponse.StatusCode;

            switch (status)
            {
                case HttpStatusCode.OK:
                    msg = "Operação realizada com sucesso.";
                    code = 200;
                    break;
                case HttpStatusCode.BadRequest:
                    msg = "Erro ao executar a operação.";
                    code = 400;
                    break;
                case HttpStatusCode.Unauthorized:
                    msg = "Erro de autorização. Verifique se o usuário solicitante tem as permissões necessárias.";
                    code = 401;
                    break;
                case HttpStatusCode.NotFound:
                    msg = "O recurso requisitado não foi encontrado.";
                    code = 404;
                    break;
                case HttpStatusCode.InternalServerError:
                    msg = "Erro de execução no servidor.";
                    code = 500;
                    break;
                default:
                    break;
            }

            RespostaApi retorno = new RespostaApi
            {
                codigo = code,
                mensagem = msg
            };

            return retorno;

        }
    }
}