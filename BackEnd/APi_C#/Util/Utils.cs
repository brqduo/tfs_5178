﻿using System.Configuration;
using System.Net;
using System;
using System.Xml.Linq;
using System.IO;
using ons.common.utilities.ExceptionHandling;
using System.Text.RegularExpressions;

namespace API.Mobile.PushNotifications.Util
{
    public class Util_service
    {
        public string GetImageBase64(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            else
            {
                byte[] data = null;
                string formato = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    try
                    {
                        using (WebClient client = new WebClient())
                        {
                            client.Credentials = GetSharepointCredential();
                            data = client.DownloadData(path);
                            formato = Path.GetExtension(path).Substring(1);

                            return "data:image/" + formato + ";base64," + Convert.ToBase64String(data);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Registrar(ex, new object { }, "IntranetRepository|GetImageBase64");
                        return null;
                    }
                }
            }
        }
        ///<summary>
        // Get Credential withou Encription.If you need to encript you must encript pass on web.config first and then change senhacharepoint to encripted
        // </summary>
        // <returns></returns>
        public NetworkCredential GetSharepointCredential()
        {
            return new NetworkCredential(
                ConfigurationManager.AppSettings.Get("usuarioSharePoint").Split('\\')[1],
                ConfigurationManager.AppSettings.Get("senhaSharePoint"),
                 ConfigurationManager.AppSettings.Get("usuarioSharePoint").Split('\\')[0]);
        }


        public string GetThumbnailUrl(String pictureUrl)
        {
            if (pictureUrl == null || pictureUrl == "")
                return "";

            Uri uri = new Uri(pictureUrl);
            string pictureName = System.IO.Path.GetFileName(uri.AbsolutePath);
            string thumbUrl = "";
            thumbUrl += "_t/";
            thumbUrl += Regex.Replace(pictureName, ".jpg", "_jpg.jpg", RegexOptions.IgnoreCase);
            thumbUrl = pictureUrl.Replace(pictureName, thumbUrl);
            return thumbUrl;

        }

    }
}


