﻿using System.Collections.Generic;
using System.Web.Http;
using API.Mobile.Contatos.Models;
using API.Mobile.PushNotifications.Util;

namespace API.Mobile.PushNotifications.Controllers
{
    [RoutePrefix("api/pushnotifications")]
    public class FirebaseController : ApiController
    {
        FirebaseService firebase = new FirebaseService();

        //[Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [Route("registrar")]
        [HttpPost]
        public RespostaApi Registrar()
        {
            return firebase.Registrar();
        }

        //[Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [Route("notificar")]
        [HttpPost]
        public RespostaApi Notificar()
        {
            return firebase.Notificar();
        }

        [Route("getTopicosByApp")]
        [HttpGet]
        public string[] getTopicosByApp(string nomeApp)
        {
            return firebase.getTopicosByApp(nomeApp);
        }


        [Route("GetUsuariosByApp")]
        [HttpGet]
        public List<Usuario> GetUsuariosByApp(string nomeApp)
        {
            return firebase.GetUsuariosByApp(nomeApp);
        }


        [Route("GetUsuariosByTopicos")]
        [HttpGet]
        public List<Usuario> GetUsuariosByTopicos([FromUri]string[] topicos)
        {
            return firebase.GetUsuariosByTopicos(topicos);
        }


        [Route("getAppList")]
        [HttpGet]
        public List<App> getAppList()
        {
            return firebase.getAppList();
        }

        [Route("addApp")]
        [HttpPost]
        public string addApp([FromBody]App app)
        {
            return firebase.addApp(app);
        }

        [Route("addAppList")]
        [HttpPost]
        public List<App> addAppList([FromBody] List<App> app)
        {
            return firebase.addAppList(app);
        }
    }
}
