﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ContatosAPI.Models;
using ContatosAPI.Util;


namespace ContatosAPI.Controllers
{
    [RoutePrefix("api/contatos")]
    public class ContatosController : ApiController
    {
        // GET api/<controller>
        // [Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
         [Route("get")]
        public IList<Ramal> Get(string tipo = "padrao")
        {
            return ContatosService.GetAll(tipo: tipo);
        }

        // GET api/<controller>
        [Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [Route("modificados")]
        [HttpGet]
        public IList<Ramal> modificados(string data = "", string tipo="padrao")
        {
            return ContatosService.GetAll(dataLimite: data, tipo: tipo);
        }

        // [Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [Route("Gerencias")]
        [HttpGet]
        public IList<gerencia> Gerencias()
        {
            return ContatosService.Gerencias();
        }

        // [Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [Route("fotos")]
        [HttpGet]
        public IList<Ramal> ContatosComFoto(string id = "")
        {
            return ContatosService.GetAll(tipo: "foto", id: id);
        }

        // [Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [Route("Interesses")]
        [HttpGet]
        public IList<string> Interesses()
        {
            return ContatosService.Interesses();
        }

        // [Authorize(Roles = "Acesso Irrestrito Contatos Internos, Acesso Restrito Contatos Internos")]
        [Route("InteressesContato")]
        [HttpGet]
        public IList<Ramal> InteressesContato(string interesse = "")
        {
            return ContatosService.InteressesContato(interesse: interesse);
        }

    }
}