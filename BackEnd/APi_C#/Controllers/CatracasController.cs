﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Mobile.Contatos.Controllers
{
    [RoutePrefix("api/catracas")]
    public class CatracasController : ApiController
    {
        // GET: api/Catracas
        public IEnumerable<string> Get()
        {
            //var client = new RestClient("http://rj-dk-273-29537.ons.org.br/ons.controleponto.iu.web/Views/Pontoonline/Get?skip=0&top=0");
            var client = new RestClient("http://nbrj026120.brq.com/ons.controleponto.iu.web/Views/Pontoonline/Get?skip=0&top=0");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "989d101c-5141-4ca5-b330-7988730a1baf");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json;charset=UTF-8");
            request.AddHeader("Cookie", ".ONSAUTH_VTPOP_01=DEFA5D97A4493F530129B3C67A7486BA104736348DD2D988787A6DAE73FCA974B3A133D57B65915D72C0DAE73A0AFC969C5971CEAF887F4695EDC4C504A91DCCC1832F604C44CC462878C9F5FA27CBF0AAB2D9FFFECC876E3D3B34AB86DD7593DF8BAEBC47E721BF4A488AACBD64660509750C60");
            request.AddParameter("undefined", "{\"FiltroAvancado\":true,\"DataRegistroPontoInicio\":\"2017-12-02T00:00:00.000Z\",\"DataRegistroPontoFim\":\"2018-05-09T00:00:00.000Z\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return new string[] { "value1", "value2" };
        }

        // GET: api/Catracas/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Catracas
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Catracas/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Catracas/5
        public void Delete(int id)
        {
        }
    }
}
