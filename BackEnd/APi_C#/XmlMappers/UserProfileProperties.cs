﻿using System.Collections.Generic;
using System.Xml.Serialization;


namespace XmlMapper
{
    public class UserProfileProperties
    {
        [XmlElement("d:element")]
        public List<Element> Elements { get; set; }
    }
}