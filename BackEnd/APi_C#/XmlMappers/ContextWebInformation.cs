﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XmlMapper
{
    [XmlRoot("d:GetContextWebInformation")]
    public class ContextWebInformation
    {
        [XmlElement("d:FormDigestValue")]
        public string FormDigestValue { get; set; }
        [XmlElement("d:FormDigestTimeoutSeconds")]
        public string FormDigestTimeoutSeconds { get; set; }
    }
}