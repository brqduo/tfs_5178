﻿using System.Xml.Serialization;


namespace XmlMapper
{
    public class Element
    {
        public string value { get; set; }
        [XmlElement("d:Key")]
        public string Key { get; set; }
        [XmlElement("d:Value")]
        public string dValue { get; set; }
    }
}