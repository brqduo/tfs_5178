﻿using System.Web.Http;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(API.Mobile.PushNotifications.Startup))]

namespace API.Mobile.PushNotifications
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Util.UserService.InitializeCache();

            HttpConfiguration config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            // app.ConfigureOAuth();

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(config);
        }
    }
}
