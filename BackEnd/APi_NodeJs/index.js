'use strict';

const admin = require("firebase-admin");
const serviceAccount = require('./key.json');
const payloadJson = require('./test.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://brq-push-test.firebaseio.com"
});
const registrationtoken = "e3ME_DQLBZc:APA91bGENhTDQfrq7fRzUDWMBNW8PckziCDit0iyZE5to4IYqTxLbdxqqd9274-Y8qhHSdxO8X6oC_cTBPGwtHm2CQiCl88Mf8jpEPtmiZ8NE0mKiXVf7EJ71ZQkPh0QV636P5pq1qQw"
const colorHexFull = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
const payload = {
  data: {
    primaryKey: payloadJson.primaryKey,
    destiny: payloadJson.destiny,
    nome: payloadJson.nome,
    callPushBadge: payloadJson.callPushBadge,
    Uid: Math.random().toString(36).substr(2, 9)
  },
  notification: {
    body: "O que temos aqui, hum?",
    tap: "true",
    color: colorHexFull,
    title: "Omega122222222",
  }
}

const options = {
  priority: 'high',
  timeToLive: 5000
};

admin.messaging().sendToDevice(registrationtoken, payload, options)
  .then((response) => {
    console.log('Funcionaaa', response);
  }).catch((err) => {
    console.log('Error', err);
  })