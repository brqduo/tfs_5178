import { Component } from '@angular/core';
import { NavController, Platform, AlertController } from 'ionic-angular';
import { Firebase } from '@ionic-native/firebase';


import { Subject } from 'rxjs/Subject';
import { tap } from 'rxjs/operators';
import { FcmProvider } from './fcm.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  badges: number = null

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public fcm: FcmProvider,
    public alertCtrl: AlertController,
    public firebaseSrv: Firebase) {
      this.testeDeJson()
    this.fcm.showBadgesNumber.asObservable().subscribe((numero) => {
      this.logBadges();
    })

  }
  startPush() {
    console.log('Dentro do push em home');
    this.firebaseSrv.setBadgeNumber(5).then((res) => {
      console.log('Res de badge', res);
    }).catch((err) => {
      console.log(err);
    })
    this.fcm.showMeBadge().then((res: any) => {
      console.log('Res de badge', res);
      this.badges = res;
    })
    this.fcm.getToken();
  }

  logBadges() {
    /* this.fcm.setBadge(100).then() */
    this.fcm.showMeBadge().then((res: any) => {
      console.log('Res de badge', res);
      this.badges = res;
    })

  }
  deleteBadges() {
    this.fcm.cleanBadge().then((res) => {
      console.log('Resultado da linha 47', res);
      this.logBadges()

    })
  }


  testeDeJson() {

    const X = {
      data: '1',
      data2: '2'
    },
      Y = {
        data: X,
        teste: 12,
        teste2: 13
      }

    console.log('Sera q isso funciona?', JSON.stringify(Y))


  }

}



